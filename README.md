CSS Userstyles
=============

### Для поинтача:

* [point.im-dark-grey.user.css][point-css]
* [point.im-dark-grey.user.js][point-js]

(скрипт просто дублирует стиль)

<http://point.im>  
![point.im dark-grey.png](point.im-dark-grey.png)  


### Для жуйца:

* [juick-dark-grey.user.css][juick-css]
* [juick-dark-grey.user.js][juick-js]

(скрипт просто дублирует стиль)

<http://juick.com>  
![juick-dark-grey.png](juick-dark-grey.png)  

[point-css]: https://gitlab.com/flashwalker/userstyles/raw/master/point.im-dark-grey.user.css
[point-js]: https://gitlab.com/flashwalker/userstyles/raw/master/point.im-dark-grey.user.js
[juick-css]: https://gitlab.com/flashwalker/userstyles/raw/master/juick-dark-grey.user.css
[juick-js]: https://gitlab.com/flashwalker/userstyles/raw/master/juick-dark-grey.user.js
